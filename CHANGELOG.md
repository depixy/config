## [1.0.2](https://gitlab.com/depixy/config/compare/v1.0.1...v1.0.2) (2020-12-16)


### Bug Fixes

* use ajv 2019 ([084e4ee](https://gitlab.com/depixy/config/commit/084e4ee916ee62f28dc2548ed3eb302b4918175c))

## [1.0.1](https://gitlab.com/depixy/config/compare/v1.0.0...v1.0.1) (2020-12-16)


### Bug Fixes

* update ajv to latest ([7938de6](https://gitlab.com/depixy/config/commit/7938de69d7f42f28bcdcdcc8dcf0d1071882e237))

# 1.0.0 (2020-12-15)


### Features

* initial commit ([9346b36](https://gitlab.com/depixy/config/commit/9346b36651c7e29df9e837c61c4eda44cd990b83))
