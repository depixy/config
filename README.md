# @depixy/config

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![NPM][npm_badge]][npm] [![semantic-release][semantic_release_badge]][semantic_release]

Config for Depixy.

## Installation

```
npm i @depixy/config
```

[license]: https://gitlab.com/depixy/config/blob/master/LICENSE
[license_badge]: https://img.shields.io/npm/l/@depixy/config
[pipelines]: https://gitlab.com/depixy/config/pipelines
[pipelines_badge]: https://gitlab.com/depixy/config/badges/master/pipeline.svg
[npm]: https://www.npmjs.com/package/@depixy/config
[npm_badge]: https://img.shields.io/npm/v/@depixy/config/latest.svg
[semantic_release]: https://github.com/semantic-release/semantic-release
[semantic_release_badge]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
