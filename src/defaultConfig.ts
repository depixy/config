import { ConfigFunction } from "./type";

const readEnvConfig: ConfigFunction = async ({ env }) => ({
  database: {
    connectionString: env.string("DEPIXY_DATABASE_URL")
  },
  storage: {
    storage: {
      type: "s3",
      option: {
        endPoint: env.string("DEPIXY_S3_END_POINT"),
        ssl: env.bool("DEPIXY_S3_USE_SSL", { defaultValue: undefined }),
        port: env.int("DEPIXY_S3_PORT", { defaultValue: undefined }),
        bucket: env.string("DEPIXY_S3_BUCKET"),
        accessKey: env.string("DEPIXY_S3_ACCESS_KEY"),
        secretKey: env.string("DEPIXY_S3_SECRET_KEY")
      }
    },
    temp: {
      type: "local",
      option: {
        baseDir: env.string("DEPIXY_TEMP_DIR")
      }
    }
  },
  port: env.int("DEPIXY_PORT", { defaultValue: 8080 }),
  cookie: {
    secret: env.string("DEPIXY_COOKIE_SECRET"),
    secure: env.bool("DEPIXY_COOKIE_SECURE", { defaultValue: true })
  }
});

export default readEnvConfig;
