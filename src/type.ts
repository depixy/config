import type { env } from "@depixy/env";

export interface Config {
  database: {
    connectionString: string;
  };
  storage: StorageOption;
  port: number;
  cookie: {
    secret: string;
    secure: boolean;
  };
}

type StorageOption = {
  storage: S3StorageOption | LocalStorageOption;
  temp: S3StorageOption | LocalStorageOption;
};

interface S3StorageOption {
  type: "s3";
  option: {
    endPoint: string;
    ssl?: boolean;
    port?: number;
    bucket: string;
    accessKey: string;
    secretKey: string;
    region?: string;
  };
}

interface LocalStorageOption {
  type: "local";
  option: {
    baseDir: string;
  };
}

export interface ConfigFunctionOption {
  env: typeof env;
}

export interface ConfigFunction {
  (opts: ConfigFunctionOption): Promise<Config>;
}
