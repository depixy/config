import { DefinedError } from "ajv";

export class ConfigError extends Error {
  errors: DefinedError[];
  constructor(errors: DefinedError[]) {
    super(`Config Error:\n ${JSON.stringify(errors, null, 2)}`);
    this.errors = errors;
  }
}
