import _ from "lodash";
import path from "path";
import { env } from "@depixy/env";

import { validateSchema } from "./valid";
import { Config } from "./type";

export const readConfig = async (): Promise<Config> => {
  const configLocation = env.string("DEPIXY_CONFIG", {
    defaultValue: path.join(__dirname, "defaultConfig")
  });
  const module = (await import(configLocation)).default;
  const config = _.isFunction(module) ? await module({ env }) : module;
  return validateSchema(config);
};
