import { DefinedError } from "ajv";
import Ajv from "ajv/dist/2019";

import { Config } from "./type";
import schema from "./option.schema.json";
import { ConfigError } from "./error";

const ajv = new Ajv();
const validate = ajv.compile<Config>(schema);

export const validateSchema = (value: unknown): Config => {
  if (validate(value)) {
    return value;
  }
  throw new ConfigError(validate.errors as DefinedError[]);
};
